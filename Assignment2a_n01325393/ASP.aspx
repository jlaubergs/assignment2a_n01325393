﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <div class="container-fluid headline bg-dark">
        <h1>ASP.NET</h1>
    </div>
</asp:Content>


<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="mb-5">
        <h2 class="mb-4">Loop for checkboxes</h2>
<pre class="bg-light rounded p-3">
<code>List&lt;string&gt; project_services = new List&lt;string&gt;();
foreach (Control control in projectServices.Controls)
{
    if (control.GetType() == typeof(CheckBox))
    {
        CheckBox service = (CheckBox)control;
        if (service.Checked)
        {
            project_services.Add(service.Text);
        }
    }
}</code></pre>
        <p>The code above is used to retrieve all checked input fields from the submitted form within a specified container.</p>
        <ol>
            <li>It creates a list type variable for storing the checked input names</li>
            <li>Within a foreach loop it goes through all controls defined within a certain container</li>
            <li>If a control is checkbox the code assigns it to a new variable</li>
            <li>Then checks if this specific input has been checked</li>
            <li>In case it is, the name of this checkbox is stored within the previously defined list variable</li>
        </ol>
        <p class="font-italic">This code example was taken from an in class example</p>
        
    </div>
    <div>
        <h2 class="mb-4">Glossary</h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Word</th>
                    <th scope="col">Explanation</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Method</th>
                    <td>A function that belongs to a class</td>
                </tr>
                <tr>
                    <th scope="row">Object</th>
                    <td>One specific instance of a class</td>

                </tr>
                <tr>
                    <th>Class</th>
                    <td>A template for an object defining fields/properties. Usually used to describe a single concept</td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="concept" runat="server">
    <h3 class="h5 mb-2">Server controls</h3>
    <p class="mb-0">Server controls are special elements that are used to render HTML content within a Webform. They are used in order to access server side processing and be able to utilise it for the functionality. Server controls often render also a more advanced functionality than simple HTML can achieve, for example Calendar server control.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="sidebar" runat="server">
    <ul class="mb-0">
        <li><a href="http://www.w3c-schools.com/aspnet/aspnet_intro.asp.htm" target="_blank">Webforms tutorial</a></li>
        <li><a href="https://getbootstrap.com/docs/4.0/getting-started/introduction/" target="_blank">Bootstrap Docs</a></li>
        <li><a href="https://msdn.microsoft.com/en-us/library/zsyt68f1.aspx" target="_blank">Server control overview</a></li>
    </ul>
</asp:Content>

<asp:Content ContentPlaceHolderID="mysection" runat="server">
    <div class="row">
        <div class="col-lg-6 align-self-center">
            <h2 class="mb-4">Constructor function</h2>
            <p>To build a public object a constructor function is normally used. This function is being defined within a class file. And can be called out by its name. In the example we can see that the function is being called out and by passing 3 variables an object is created containing these variables.</p>
        </div>
        <div class="col-lg-6 align-self-center">
<pre class="bg-light border border-dark rounded p-3 mb-0">
<code>Inquiry submitedInquiry = new Inquiry(inquiryCompany, inquiryContact, inquiryProject);

// * Class File
public Company inquiryCompany;
public Contact inquiryContact;
public Project inquiryProject;

public Inquiry(Company obCompany, Contact obContact, Project obProject)
{
    inquiryCompany = obCompany;
    inquiryContact = obContact;
    inquiryProject = obProject;
}</code></pre>
        </div>
    </div>
    
</asp:Content>
