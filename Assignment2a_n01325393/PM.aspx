﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <div class="container-fluid headline bg-info">
        <h1>Project Management</h1>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div>
        <h2 class="mb-4">Website Project Parts</h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Part</th>
                    <th scope="col">Involves</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Backend</th>
                    <td>Database, Security, Server-side languages (C#)</td>
                </tr>
                <tr>
                    <th scope="row">Front-end</th>
                    <td>User Interface, User Experience, Branding, Interactions, Accessibility</td>
                </tr>
                <tr>
                    <th>Content</th>
                    <td>Text, Copy, Images, Products, Services, Information Architecture</td>
                </tr>
                <tr>
                    <th>Strategy</th>
                    <td>Placement, Target audience, Features, Maintanance</td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="concept" runat="server">
    <h3 class="h5 mb-2">RFP</h3>
    <p class="mb-0">RFP stands for Request for Proposal, which usually is a documentation that a company interested in vendors sends out in order to receive proposals for a project they have defined.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="sidebar" runat="server">
    <ul class="mb-0">
        <li><a href="https://www.wikihow.com/Write-a-Proposal" target="_blank">Proposal Writing</a></li>
        <li><a href="https://www.pandadoc.com/digital-marketing-proposal-template/" target="_blank">Proposal Example</a></li>
    </ul>
</asp:Content>

<asp:Content ContentPlaceHolderID="mysection" runat="server">
    <div class="row">
        <div class="col-lg-8">
            <h2 class="mb-4">Proposal</h2>
            <p>A proposal is usually a written document stating what will be provided by a company for a project. Its goal is to sell a project and put it forward for consideration without giving free consulation advice. Proposal usually includes scope of the project, adviced solutions and their benefits and a budget.</p>
        </div> 
    </div>
</asp:Content>