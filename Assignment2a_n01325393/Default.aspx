﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <div class="container-fluid headline bg-primary">
        <h1>Learning Site</h1>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="concept" runat="server">
    <h3 class="h5 mb-2">Tricky Concept</h3>
    <p class="mb-0">This box is going to be used to describe a tricky concept learned within a certain class.</p>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <p>This site has been created for learning purposes. It covers different concepts from 3 classes I have been taught on my exchange program. The covered classes are:</p>
    <div class="row">
        <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
            <div class="bg-dark rounded">
                <a class="d-block text-white p-3" href="ASP.aspx">ASP.NET</a>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
            <div class="bg-danger rounded">
                <a class="d-block text-white p-3" href="Design.aspx">Design</a>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
            <div class="bg-info rounded">
                <a class="d-block text-white p-3" href="PM.aspx">Project Management</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="sidebar" runat="server">
    <ul class="mb-0">
        <li><a href="https://login.humber.ca" target="_blank">Blackboard</a></li>
        <li><a href="https://humber.ca/myhumber/" target="_blank">My humber</a></li>
    </ul>
</asp:Content>


<asp:Content ContentPlaceHolderID="mysection" runat="server">
    <div class="row">
        <div class="col-lg-8">
            <h2 class="mb-4">Solution Description</h2>
            <p>This section of the site is going to present a description of a specific solution in the same time describing a concept learned within a class. If a personal example exists, it will also be provided.</p>
        </div> 
    </div>
</asp:Content>
