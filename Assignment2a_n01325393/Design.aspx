﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <div class="container-fluid headline bg-danger">
        <h1>Design</h1>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="content" runat="server">
    <div>
        <h2 class="mb-4">UI Design Principles </h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Pattern</th>
                    <th scope="col">Key-points</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Clarity</th>
                    <td>
                        <ul>
                            <li>Makes users confident about their actions</li>
                            <li>Doesn't make the user think</li>
                            <li>Is not always simplicity</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Flexibility</th>
                    <td>
                        <ul>
                            <li>Looks good in all situations</li>
                            <li>Works within different contexts</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>Familiarity</th>
                    <td>
                        <ul>
                            <li>Use of already established design elements</li>
                            <li>Involves common practices</li>
                            <li>Don't make the user think</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>Efficiency</th>
                    <td>
                        <ul>
                            <li>UI should help tasks to be done effectively</li>
                            <li>Help user by using hints, tips or errors - don't over do it</li>
                            <li>Design taking in mind users needs and challenges</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>Consistency and structure</th>
                    <td>
                        <ul>
                            <li>Start building familiarity</li>
                            <li>Cleans up the design</li>
                            <li>Creates visual hierarchy</li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="concept" runat="server">
    <h3 class="h5 mb-2">Signifiers, Affordances, Constraints</h3>
    <ul>
        <li>Affordances describe what possible interactions can user afford.</li>
        <li>Signifiers are elements the describe where and how an action can be pursued.</li>
        <li>Constraints usually together with feedback informs a user of what is not possible to do, or in other words doesn't allow to perform certain actions.</li>
    </ul>
</asp:Content>

<asp:Content ContentPlaceHolderID="sidebar" runat="server">
    <ul class="mb-0">
        <li><a href="https://simpleicons.org/" target="_blank">Simple Icons</a></li>
        <li><a href="https://pttrns.com/" target="_blank">Mobile Design Patterns</a></li>
        <li><a href="https://refactoringui.com" target="_blank">UI Design Tips</a></li>
    </ul>
</asp:Content>

<asp:Content ContentPlaceHolderID="mysection" runat="server">
    <div class="row">
        <div class="col-lg-6 align-self-center">
            <h2 class="mb-4">Style tile</h2>
            <p>A style tile is being used at an early stage of a project in order to present a style of an upcoming website. It takes less time and makes it clear to a client what will be style considerations. This is good as the project is not being designed further while not knowing whether client is happy with the direction.</p>
        </div>
        <div class="col-lg-6 align-self-center">
            <img class="w-100" src="Images/stylesheet.png" alt="Stylesheet" />
        </div>
    </div>
</asp:Content>